import { TestBed, async, inject } from '@angular/core/testing';

import { StudentAndLecturerGuard } from './student-and-lecturer.guard';

describe('StudentAndLecturerGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentAndLecturerGuard]
    });
  });

  it('should ...', inject([StudentAndLecturerGuard], (guard: StudentAndLecturerGuard) => {
    expect(guard).toBeTruthy();
  }));
});
